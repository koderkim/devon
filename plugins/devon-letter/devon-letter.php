<?php
/*
 * Plugin Name: Devon letter
 * Description: Change user register letter
 * Version: 1.0
 * Author: Piligrim
 * Author URI: http://piligrim.pro
 * License: GPLv2 or later
 */
if(!function_exists('wp_new_user_notification')){
	function wp_new_user_notification($user_id, $plaintext_pass = ''){
		$user = new WP_User($user_id);
		$user_login = stripslashes($user->user_login);
		$user_email = stripslashes($user->user_email);

		$message = sprintf(__('A new user is registered on your site %s:'), get_option('blogname')) . "\r\n\r\n";
		$message .= sprintf(__('Login: %s'), $user_login) . "\r\n\r\n";
		$message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";
		@wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);

		//if(empty($plaintext_pass)) return;

		$message = __('Hi.') . "\r\n\r\n";
		$message .= sprintf(__("Thank you for posting to the site %s! You can now log in using this data:"), get_option('blogname')) . "\r\n\r\n";
		$message .= wp_login_url() . "\r\n";
		$message .= sprintf(__('Login: %s'), $user_login) . "\r\n";
		$message .= sprintf(__('Password: %s'), $plaintext_pass) . "\r\n\r\n";
		$message .= sprintf(__('If you have any problems with registration or login, contact the administrator - %s.'), get_option('admin_email')) . "\r\n\r\n";
		wp_mail($user_email, sprintf(__('[%s] Your name and password'), get_option('blogname')), $message);
	}
}
?>