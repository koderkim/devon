<?php get_header(); ?>
	<?php if(have_posts()): ?>
		<?php while(have_posts()): the_post(); ?>
			<?php $gallery_list = explode(',', get_field('model_gallery')); ?>
		<!-- Breadcrumbs -->
		<div class="container">
			<div class="wrapper-breadcrumbs">
				<div id="breadcrumbs">
					<a href="#">Models</a>
					<i class="icon ion-android-arrow-forward separator"></i>
					<span class="breadcrumb-last"><?php the_title(); ?></span>
				</div>
				<?php if(count($gallery_list) > 1 && strlen($gallery_list[0])): ?>
				<nav class="profile-nav-link">
					<a href="#" class="previus">
						<i class="icon ion-ios-arrow-thin-left"></i>
					</a>
					<a href="#" class="next">
						<i class="icon ion-ios-arrow-thin-right"></i>
					</a>
				</nav>
				<?php endif; ?>
			</div>
		</div>
		<!-- End Breadcrumbs -->
		<!-- Model Profile -->
		<div class="profile-entry container">
			<?php if(count($gallery_list) > 0 && strlen($gallery_list[0])): ?>
			<div class="profile-photos">
				<div class="row-sm">
					<div class="col-lg-6 profile-photos-container">
					<?php foreach($gallery_list as $id): ?>
						<?php $image = wp_get_attachment_image_src($id, 'full'); ?>
						<img class="main-photo" src="<?php echo $image[0]; ?>" alt="<?php echo get_post_meta($id, '_wp_attachment_image_alt', true); ?>" title="<?php echo get_the_title($id); ?>">
					<?php endforeach; ?>
					</div>
					<div class="col-lg-6 thumbnail-photo">
						<div class="row-sm justify-content-center">
							<?php foreach($gallery_list as $id): ?>
							<div class="col-6 col-md-4">
								<?php $thumb = wp_get_attachment_image_src($id, 'medium'); ?>
								<img src="<?php echo $thumb[0]; ?>" alt="<?php echo get_post_meta($id, '_wp_attachment_image_alt', true); ?>">
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<section class="col-md-9">
					<div class="profile-info">
						<header class="profile-header">
							<h2 class="profile-title"><?php the_title(); ?></h2>
							<p class="rating">
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
							</p>
						</header>
						<div class="model-attr">
							<?php if(get_field('model_location')): ?>
							<p class="attr-item">
								<span class="attr-name">Location:</span>
								<span class="attr-value"><?php echo get_field('model_location'); ?></span>
							</p>
							<?php endif; ?>
							<?php if(get_field('model_background')): ?>
							<p class="attr-item">
								<span class="attr-name">Background:</span>
								<span class="attr-value"><?php echo get_field('model_background'); ?></span>
							</p>
							<?php endif; ?>
							<?php if(get_field('model_body')): ?>
							<p class="attr-item">
								<span class="attr-name">Body:</span>
								<span class="attr-value"><?php echo get_field('model_body'); ?></span>
							</p>
							<?php endif; ?>
							<?php if(get_field('model_donation')): ?>
							<p class="attr-item">
								<span class="attr-name">Donation:</span>
								<span class="attr-value"><?php echo get_field('model_donation'); ?></span>
							</p>
							<?php endif; ?>
						</div>
						<div class="model-description">
							<?php the_content(); ?>
						</div>
					</div>
				</section>
				<aside class="col-md-3">
					<p class="soc-icons">
						<a href="#"><i class="fa fa-facebook-f"></i></a>
						<a href="#"><i class="fa fa-instagram"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
					</p>
					<?php /* ?><a href="#" class="btn-classic">Send Privat message</a><?php */ ?>
				</aside>
			</div>
		</div>
		<?php endwhile; ?>
	<?php endif; ?>
		<!-- End Model Profile -->
		<!-- Reviews -->
		<?php /* ?><div class="profile-reviews container">
			<div class="row">
				<aside class="col-lg-3 sidebar">
					<div class="model-contacts">
						<h3 class="title">Location</h3>
						<div id="map-model-location"></div>
						<p class="place">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
							<span>168 Luckie St NW, Atlantas</span>
						</p>
						<p class="phone">
							<i class="fa fa-mobile" aria-hidden="true"></i>
							<a href="#">+1 212-380-1195</a>
						</p>
					</div>
					<a href="#" class="wr-ads">
						<p>Ads</p>
						<p>260 x 300</p>
					</a>
					<div class="another-top-models">
						<h3 class="title">Featured Models</h3>
						<div class="another-model-slider">
							<a href="05_Model_Profile.html" class="item-wr">
								<div  class="model-item" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_900x500.jpg')">
									<div class="model-info">
										<p>Height: <span>185</span></p>
										<p>Bust: <span>85</span></p>
										<p>Waist: <span>60</span></p>
										<p>Age: <span>17</span></p>
										<p>Hair: <span>Red</span></p>
										<p>Eyes: <span>Blue</span></p>
										<p class="rating">
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
										</p>
									</div>
								</div>
							</a>
							<a href="05_Model_Profile.html" class="item-wr">
								<div  class="model-item" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_900x500.jpg')">
									<div class="model-info">
										<p>Height: <span>185</span></p>
										<p>Bust: <span>85</span></p>
										<p>Waist: <span>60</span></p>
										<p>Age: <span>17</span></p>
										<p>Hair: <span>Red</span></p>
										<p>Eyes: <span>Blue</span></p>
										<p class="rating">
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
										</p>
									</div>
								</div>
							</a>
							<a href="05_Model_Profile.html" class="item-wr">
								<div  class="model-item" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_900x500.jpg')">
									<div class="model-info">
										<p>Height: <span>185</span></p>
										<p>Bust: <span>85</span></p>
										<p>Waist: <span>60</span></p>
										<p>Age: <span>17</span></p>
										<p>Hair: <span>Red</span></p>
										<p>Eyes: <span>Blue</span></p>
										<p class="rating">
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
										</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</aside>
				<section class="col-lg-9 order-lg-first">
					<h3 class="section-title lines">Recent Reviews</h3>
					<div class="rewiew-section">
						<div class="all-review">
							<article class="blog-comment">
								<div class="author-photo-wr">
									<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_80x80.jpg" alt="">
								</div>
								<div class="author-content">
									<header class="comment-header">
										<h3 class="author-name">Marie Hoola</h3>
										<p class="rating">
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
										</p>
										<p class="publish">15 December, 2017</p>

									</header>
									<div class="block-content">
										<p>Proin dignissim magna ut varius faucibus. Suspendisse aliquet, risus id hendrerit tincidunt mau risnisi ultrices nisl, et consectetur tellus est sed justo. Orci varius natoque penatibus etmag.</p>
									</div>
								</div>
							</article>
							<article class="blog-comment">
								<div class="author-photo-wr">
									<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_80x80.jpg" alt="">
								</div>
								<div class="author-content">
									<header class="comment-header">
										<h3 class="author-name">Perry Powell</h3>
										<p class="rating">
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
										</p>
										<p class="publish">12 December, 2017</p>

									</header>
									<div class="block-content">
										<p>Proin dignissim magna ut varius faucibus. Suspendisse aliquet, risus id hendrerit tincidunt.</p>
									</div>
								</div>
							</article>
							<article class="blog-comment">
								<div class="author-photo-wr">
									<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_80x80.jpg" alt="">
								</div>
								<div class="author-content">
									<header class="comment-header">
										<h3 class="author-name">Ross Hender</h3>
										<p class="publish">09 December, 2017</p>

									</header>
									<div class="block-content">
										<p>Proin dignissim magna ut varius faucibus. Suspendisse aliquet, risus id hendrerit tincidunt, mauris nisi ultrices nisl, et consectetur tellus est sed justo. Orci varius natoque penatibus et magnis dis parturient montes, nasc etur ridiculus mus. Vestibulum a pulvinar quam.</p>
									</div>
								</div>
							</article>
							<article class="blog-comment">
								<div class="author-photo-wr">
									<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_80x80.jpg" alt="">
								</div>
								<div class="author-content">
									<header class="comment-header">
										<h3 class="author-name">Hughes Flores</h3>
										<p class="rating">
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star active" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
										</p>
										<p class="publish">15 December, 2017</p>

									</header>
									<div class="block-content">
										<p>Proin dignissim magna ut varius faucibus. Suspendisse aliquet, risus id hendrerit tincidunt mau risnisi ultrices nisl, et consectetur tellus est sed justo. Orci varius natoque penatibus etmag.</p>
									</div>
								</div>
							</article>
						</div>
						<form id="rate-form" class="form rate-form">
							<p class="title">Write Review</p>
							<p class="form-message error">Message should not be empty</p>
							<p class="form-message saccess">Message sent saccessfuly</p>
							<div class="rate-model">
								<p class="rating">
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
								</p>
								<p class="hidden" id="rating-value">
									<input type="radio" name="rating" value="1">
									<input type="radio" name="rating" value="2">
									<input type="radio" name="rating" value="3">
									<input type="radio" name="rating" value="4">
									<input type="radio" name="rating" value="5">
								</p>
								<span>Rate Model</span>
							</div>
							<div class="row-input">
								<div class="item-wr">
									<input type="text" name="name" placeholder="Your Name" class="name">
								</div>
								<div class="item-wr">
									<input type="email" name="email" placeholder="Your Email" class="email">
								</div>
							</div>
							<textarea name="review-message" placeholder="Message"></textarea>
							<input type="submit" value="Post Review" class="btn-classic">
						</form>
					</div>
				</section>
			</div>
		</div><?php */ ?>
		<!-- End Reviews -->
<?php get_footer(); ?>