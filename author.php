<?php
	session_start();
	// Messages
	$message = array(
		'info' => '',
		'error' => ''
	);
	if(isset($_SESSION['message'])){
		$message = $_SESSION['message'];
	}

	// Is user logged
	if(is_user_logged_in() && isset($_POST) && isset($_POST['act'])){
		switch($_POST['act']){
			case 'logout':
				wp_logout();
				$message['info'] = 'Logout';
				$_SESSION['message'] = $message;
				wp_redirect($_SERVER['HTTP_REFERER']);
				exit;
				break;
		}
	}
	
	// Is user NO logged
	if(!is_user_logged_in() && isset($_POST) && isset($_POST['act'])){
		switch($_POST['act']){
			case 'login':
				$creds = array();
				$creds['user_login'] = $_POST['login'];
				$creds['user_password'] = $_POST['password'];
				$creds['remember'] = true;

				$user = wp_signon($creds, false);

				if(is_wp_error($user)){
				   	$message['error'] = $user->get_error_message();
				} else {
					wp_redirect($_SERVER['HTTP_REFERER']);
					exit;
				}
				break;
			case 'signup':
				$user_login = isset($_POST['login']) ? $_POST['login'] : '';
				$user_email = isset($_POST['email']) ? $_POST['email'] : '';
				$user_password = isset($_POST['password']) ? $_POST['password'] : '';

				$user_id = username_exists($user_login);
				if(!$user_id){
					//$random_password = wp_generate_password(12, false);
					if(mb_strlen($user_password) < 8){
						$message['error'] = __('Password must be at least 8 characters long.');
					} else {
						$random_password = $user_password;
						$user_id = wp_create_user($user_login, $random_password, $user_email);
						$message['info'] = __('Send email');
						$_SESSION['message'] = $message;
						wp_new_user_notification($user_id, $random_password);
						wp_redirect($_SERVER['HTTP_REFERER']);
						exit;
					}
				} else {
					$message['error'] = __('User already exists. Password inherited.');
				}
				break;
		}
	}

	function reArrayFiles(&$file_post){
		$file_ary = array();
		$file_count = count($file_post['name']);
		$file_keys = array_keys($file_post);
		for ($i=0; $i<$file_count; $i++){
		    foreach ($file_keys as $key){
		        $file_ary[$i][$key] = $file_post[$key][$i];
		    }
		}
		return $file_ary;
	}

	function upload_user_file($file = array()){
		require_once(ABSPATH . 'wp-admin/includes/admin.php');
		$file_return = wp_handle_upload($file, array('test_form' => false));
		if(isset($file_return['error']) || isset($file_return['upload_error_handler'])){
		  	return false;
		} else {
		  	$filename = $file_return['file'];
		  	$attachment = array(
				'post_mime_type' => $file_return['type'],
				'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
				'post_content' => '',
				'post_status' => 'inherit',
				'guid' => $file_return['url']
		  	);
		  	$attachment_id = wp_insert_attachment($attachment, $file_return['url']);
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);
			wp_update_attachment_metadata($attachment_id, $attachment_data);
			if(0 < intval($attachment_id)){
				return $attachment_id;
			}
		}
		return false;
	}
?>
<?php
// Template name: Author page
?>

<?php get_header(); ?>
		<?php if(is_user_logged_in()): ?>
		<?php
			// Current User
			$curauth = get_user_by('ID', get_current_user_id());

			// Submit form
			if(isset($_POST) && isset($_POST['act'])){
				switch($_POST['act']){
					case 'save':
						$post_data = array();
						if($_POST['id'] > 0){
							$post_data = array(
								'ID' => $_POST['id'],
								'post_type' => 'model',
								'post_title' => trim($_POST['advertisement-title']),
								'post_content' => $_POST['model-description'],
								'post_status' => 'publish',
								'post_author' => $curauth->ID,
								'tax_input' => array(
									'modelcategories' => array($_POST['model-category'])
								)
							);
							$post_id = wp_update_post($post_data);
						} else{
							$post_data = array(
								'post_type' => 'model',
							  	'post_title' => trim($_POST['advertisement-title']),
							  	'post_content' => $_POST['model-description'],
							  	'post_status' => 'publish',
							  	'post_author' => $curauth->ID,
							  	'tax_input' => array(
									'modelcategories' => array($_POST['model-category'])
								)
							);
							$post_id = wp_insert_post($post_data);
						}
						if(is_wp_error($post_id)){
							$message['error'] = $post_id->get_error_message();
						} else {
							wp_set_post_terms($post_id, intval($_POST['model-category']), 'modelcategories', false);
							update_field('model_location', $_POST['model-location'], $post_id);
							update_field('model_background', $_POST['model-background'], $post_id);
							update_field('model_body', $_POST['model-body'], $post_id);
							update_field('model_donation', $_POST['model-donation'], $post_id);

							$attachment_list = array();
							if(!empty($_FILES)){
								$files_arr = reArrayFiles($_FILES['gallery']);
								foreach($files_arr as $file){
									if(is_array($file)){
										$attachment_id = upload_user_file($file);
										if($attachment_id){
											$attachment_list[] = $attachment_id;
										}
									}
								}
							}
							$gallery_list = explode(',', get_field('model_gallery', $post_id));
							if(count($gallery_list) > 0 && strlen($gallery_list[0])){
								$attachment_list = array_merge($gallery_list, $attachment_list);
							}
							update_field('model_gallery', implode(',', $attachment_list), $post_id);

							$message['info'] = 'Update successfull!';
						}
						break;
				}
			}
			
			$model = array(
				'id' => 0,
				'advertisement-title' => '',
				'model-name' => $curauth->nickname,
				'model-email' => $curauth->user_email,
				'model-location' => '',
				'model-background' => '',
				'model-body' => '',
				'model-donation' => '',
				'model-category' => '',
				'model-description' => '',
				'model-gallery' => '',
			);
			$args = array(
				'post_type' => 'model',
				'author' =>  $curauth->ID,
				'orderby' =>  'post_date',
				'order' =>  'ASC',
				'posts_per_page' => 1
			);
			$query = new WP_Query($args);
			if($query->have_posts()):
				while($query->have_posts()): $query->the_post();
					$term = '';
					$modelTerms = get_the_terms(get_the_ID(), 'modelcategories');
					if($modelTerms){
						$term = array_shift($modelTerms);	
					}
					$model['id'] = get_the_ID();
					$model['advertisement-title'] = get_the_title();
					$model['model-location'] = get_field('model_location');
					$model['model-background'] = get_field('model_background');
					$model['model-body'] = get_field('model_body');
					$model['model-donation'] = get_field('model_donation');
					$model['model-category'] = $term ? $term->term_id : '';
					$model['model-description'] = get_the_content();
					$model['model-gallery'] = get_field('model_gallery');
				endwhile;
			endif;
			wp_reset_postdata();

			$terms = get_terms([
				'taxonomy' => 'modelcategories',
				'hide_empty' => false,
			]);
		?>
		<section class="container">
			<?php if($message['info']): ?>
			<div class="message-box message-info"><?php echo $message['info']; ?><i class="message-box-close">&times;</i></div>
			<?php endif; ?>
			<?php if($message['error']): ?>
			<div class="message-box message-error"><?php echo $message['error']; ?><i class="message-box-close">&times;</i></div>
			<?php endif; ?>
			<h2 class="section-title lines">About: <?php echo $curauth->nickname; ?></h2>
			<div id="logout">
				<form action="" method="POST">
					<input type="hidden" name="act" value="logout">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 56 56" style="width:24px;height:24px;vertical-align:top"><path fill="#ffffff" d="M54.424,28.382c0.101-0.244,0.101-0.519,0-0.764c-0.051-0.123-0.125-0.234-0.217-0.327L42.208,15.293c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414L51.087,27H20.501c-0.552,0-1,0.447-1,1s0.448,1,1,1h30.586L40.794,39.293c-0.391,0.391-0.391,1.023,0,1.414C40.989,40.902,41.245,41,41.501,41s0.512-0.098,0.707-0.293l11.999-11.999C54.299,28.616,54.373,28.505,54.424,28.382z"/><path fill="#ffffff" d="M36.501,33c-0.552,0-1,0.447-1,1v20h-32V2h32v20c0,0.553,0.448,1,1,1s1-0.447,1-1V1c0-0.553-0.448-1-1-1h-34c-0.552,0-1,0.447-1,1v54c0,0.553,0.448,1,1,1h34c0.552,0,1-0.447,1-1V34C37.501,33.447,37.053,33,36.501,33z"/></svg>
					<input type="submit" value="Logout">
				</form>
			</div>
			<div class="justify-content-center">
				<form action="" method="POST" class="row form-author" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $model['id']; ?>">
					<input type="hidden" name="act" value="save">
					<div class="col-md-4">
						<div id="upload-container">
							<input type="file" name="gallery[]" id="real-input" multiple>
							<div class="browse-btn">Browse Files</div>
							<span class="file-info">Upload images</span>
						</div>
						<div class="photo-grid">
							<?php
								$gallery_list = explode(',', $model['model-gallery']);
								if(count($gallery_list) > 0 && strlen($gallery_list[0])):
									foreach($gallery_list as $id){
							?>
								<div>
									<div class="photo-grid-cell">
										<?php $thumb = wp_get_attachment_image_src($id, 'medium'); ?>
										<img src="<?php echo $thumb[0]; ?>" alt="<?php echo get_post_meta($id, '_wp_attachment_image_alt', true); ?>">
									</div>
								</div>
							<?php
									}
								endif;
							?>
						</div>
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-12">
								<label for="advertisement-title">Advertisement Title</label>
								<input type="text" name="advertisement-title" id="advertisement-title" value="<?php echo $model['advertisement-title']; ?>" placeholder="Advertisement Title">
							</div>
							<div class="col-sm-6">
								<label for="model-name">Name</label>
								<input type="text" name="model-name" id="model-name" value="<?php echo $model['model-name']; ?>"<?php echo $model['model-name'] ? ' disabled' : ''; ?> placeholder="Your Name">
							</div>
							<div class="col-sm-6">
								<label for="model-email">Email</label>
								<input type="text" name="model-email" id="model-email" value="<?php echo $model['model-email']; ?>"<?php echo $model['model-email'] ? ' disabled' : ''; ?> placeholder="Your Email">
							</div>
							<div class="col-sm-6">
								<label for="model-location">Location</label>
								<select name="model-location" id="model-location" placeholder="Your location">
									<?php $items = acf_get_field('model_location'); ?>
									<?php foreach($items['choices'] as $key => $val): ?>
									<option value="<?php echo $key; ?>"<?php echo $key == $model['model-location'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-6">
								<label for="model-background">Background</label>
								<select name="model-background" id="model-background" placeholder="Your background">
									<?php $items = acf_get_field('model_background'); ?>
									<?php foreach($items['choices'] as $key => $val): ?>
									<option value="<?php echo $key; ?>"<?php echo $key == $model['model-background'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-6">
								<label for="model-body">Body</label>
								<select name="model-body" id="model-body" placeholder="Your body">
									<?php $items = acf_get_field('model_body'); ?>
									<?php foreach($items['choices'] as $key => $val): ?>
									<option value="<?php echo $key; ?>"<?php echo $key == $model['model-body'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-6">
								<label for="model-donation">Donation</label>
								<select name="model-donation" id="model-donation" placeholder="Your donation">
									<?php $items = acf_get_field('model_donation'); ?>
									<?php foreach($items['choices'] as $key => $val): ?>
									<option value="<?php echo $key; ?>"<?php echo $key == $model['model-donation'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-6">
								<label for="model-category">Category</label>
								<select name="model-category" id="model-category">
								<?php foreach($terms as $term): ?>
									<option value="<?php echo $term->term_id; ?>"<?php echo $model['model-category'] == $term->term_id ? ' selected' : ''; ?>><?php echo $term->name; ?></option>
								<?php endforeach; ?>
								</select>
							</div>
							<div class="col-sm-6"></div>
							<div class="col-sm-6"></div>
							<div class="col-sm-6"></div>
							<div class="col-12">
								<label for="model-description">Description</label>
								<textarea name="model-description" id="model-description" placeholder="Description"><?php echo $model['model-description']; ?></textarea>
							</div>
						</div>
					</div>
					<div class="col-md-4">&nbsp;</div>
					<div class="col-md-8">
						<input type="submit" value="Send Ad">
					</div>
				</form>
			</div>
		</section>
		<?php else: ?>
		<section class="container">
			<?php if($message['info']): ?>
			<div class="message-box message-info"><?php echo $message['info']; ?><i class="message-box-close">&times;</i></div>
			<?php endif; ?>
			<?php if($message['error']): ?>
			<div class="message-box message-error"><?php echo $message['error']; ?><i class="message-box-close">&times;</i></div>
			<?php endif; ?>
			<!-- Forms -->
			<div class="sign-form-wr">				
				<div class="sign-form-inner">
					<div id="sign-form">
						<header class="sign-header">
							<div class="log-in tab active" data-form="log-in-form">Log In</div>
							<div class="sign-up tab" data-form="sign-up-form">Sign Up</div>
						</header>
					</div>
					<!-- Log In -->
					<div class="form-wr log-in-form active">
						<p class="title">Log in</p>
						<form action="" id="sign-form" method="POST">
							<input type="hidden" name="act" value="login">
							<input type="text" name="login" value="<?php echo $_POST['login'] ? $_POST['login'] : ''; ?>" placeholder="Your Name" class="login" required>
							<input type="password" name="password" value="<?php echo $_POST['password'] ? $_POST['password'] : ''; ?>" placeholder="Password" class="password" required>
							<p class="submit">
								<input type="submit" value="Log In" class="submit-btn">
								<a href="#" class="forgot-password">Forgot Password?</a>
							</p>
						</form>
						<?php /* ?><p class="lines section-title">Or</p>
						<a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i>Sign in with facebook</a><?php */ ?>
					</div>
					<!-- End Log In -->
					<!-- Sign In -->
					<div class="form-wr sign-up-form">
						<p class="title">Sign Up</p>
						<form action="" id="sign-form" method="POST">
							<input type="hidden" name="act" value="signup">
							<input type="text" name="login" value="<?php echo $_POST['login'] ? $_POST['login'] : ''; ?>" placeholder="Your Name" class="login" required>
							<input type="email" name="email" value="<?php echo $_POST['email'] ? $_POST['email'] : ''; ?>" placeholder="Your Email" class="email" required>
							<input type="password" name="password" value="<?php echo $_POST['password'] ? $_POST['password'] : ''; ?>" placeholder="Your Password" class="password" required>
							<p class="submit">
								<input type="submit" value="Create Acoount" class="submit-btn">
							</p>
						</form>
						<?php /* ?><p class="lines section-title">Or</p>
						<a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i>Sign in with facebook</a><?php */ ?>
					</div>
					<!-- End Sign In -->
				</div>
			</div>
			<!-- End Forms -->
		</section>
		<?php endif; ?>
		<?php unset($_SESSION['message']); ?>
<?php get_footer(); ?>