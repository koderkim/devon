<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(' '); ?><?php if(wp_title(' ', false)) { echo ' &raquo; '; } ?><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/bootstrap-grid.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/bootstrap-dropdown.min.css" />
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/bootstrap-select.min.css" />
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/font-awesome.css">
	<!-- Ionicons Icons -->
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/ionicons.min.css">
	<!-- Google Fonts -->
    <link href="<?php echo get_bloginfo('template_url'); ?>/assets/fonts/main-fonts.css" rel="stylesheet">
	<!-- Slick Slider CSS -->
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/slick/slick.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/slick/slick-theme.css">
	<!-- Custom styles for this template -->
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/css/style.css">
<?php wp_head(); ?>
</head>
<body>
	<!-- Navbar -->
	<header class="header transparent">
		<div class="container">
			<div class="header-inner">
				<a href="<?php echo home_url('/'); ?>" class="site-logo">
                    <h1 class="site-logo-title"><?php echo get_bloginfo('name'); ?></h1>
                    <h1 class="site-logo-text"><?php echo get_bloginfo('description'); ?></h1>
				</a>
				<div class="navigation-wr">
					<nav class="navigation">
					<?php
						$args = array(
						  'theme_location' => 'mainmenu',
						  'container' => false
						);
						wp_nav_menu($args);
					?>
					</nav>
					<div class="right-sec-nav d-flex align-items-center">
						<form class="search-wr" action="<?php echo home_url('/'); ?>">
							<input type="text" id="s" name="s" value="<?php echo get_search_query(); ?>">
							<i class="icon ion-ios-search"></i>
							<input type="submit" class="submit" value="">
						</form>
						<a href="/my/" class="login d-flex align-items-center">
							<i class="icon ion-ios-person-outline"></i>
						</a>
						<?php /* ?><div class="lang-menu-wr">
							<button class="btn lang-toggle">
								<span>EN</span>
								<i class="icon ion-ios-arrow-down"></i>
							</button>
							<ul class="lang-menu">
								<li><a href="#">EN</a></li>
								<li><a href="#">HR</a></li>
								<li><a href="03_Home_rtl.html">AR</a></li>
							</ul>
						</div><?php */ ?>
					</div>
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>	
				<i class="fa fa-bars" aria-hidden="true"></i>
			</div>
		</div>	
	</header>
	<!-- End Navbar -->
	<main>