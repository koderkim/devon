	</main>
	<!-- Footer -->
	<footer class="site-footer">
		<div class="container">
			<div class="soc-icons-wrap">
			<?php if(get_theme_mod('devon_social_fb')): ?>
				<a href="<?php echo addhttp(get_theme_mod('devon_social_fb')); ?>" class="soc-icon"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			<?php endif; ?>
			<?php if(get_theme_mod('devon_social_instagram')): ?>
				<a href="<?php echo addhttp(get_theme_mod('devon_social_instagram')); ?>" class="soc-icon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
			<?php endif; ?>
			<?php if(get_theme_mod('devon_social_tw')): ?>
				<a href="<?php echo addhttp(get_theme_mod('devon_social_tw')); ?>" class="soc-icon"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<?php endif; ?>
			<?php if(get_theme_mod('devon_social_pinterest')): ?>
				<a href="<?php echo addhttp(get_theme_mod('devon_social_pinterest')); ?>" class="soc-icon"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
			<?php endif; ?>
			</div>
			<p class="copyright">&#169; 2018 Devon</p>
		</div>
	</footer>
	<!-- End Footer -->
	<!-- Jquery core -->
	<script src="<?php echo get_bloginfo('template_url'); ?>/assets/js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo get_bloginfo('template_url'); ?>/assets/js/bootstrap-dropdown.min.js"></script>
        <script src="<?php echo get_bloginfo('template_url'); ?>/assets/js/bootstrap-select.min.js"></script>
	<!-- Slick Slider Core -->
	<script src="<?php echo get_bloginfo('template_url'); ?>/assets/slick/slick.js"></script>
	<!-- All Sliders Initialization -->
	<script src='<?php echo get_bloginfo('template_url'); ?>/assets/js/slider.js'></script>
	<!-- Custom -->
	<script src='<?php echo get_bloginfo('template_url'); ?>/assets/js/common.js'></script>
<?php wp_footer(); ?>
</body>
</html>