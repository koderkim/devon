<?php get_header(); ?>
		<section class="container last-posts small">
		<?php if(have_posts()): ?>
			<h2 class="section-title lines"><?php echo single_cat_title(); ?></h2>
			<div class="row justify-content-center">
			<?php while(have_posts()): the_post(); ?>
				<article class="col-md-6 col-lg-4 last-post-wr">
					<a href="<?php the_permalink(); ?>" class="post-item no-decoration">
						<?php $bg = get_the_post_thumbnail_url(); ?>
						<figure class="post-image" style="background-image: url('<?php echo $bg ? $bg : get_bloginfo('template_url').'/assets/img/placeholder/placeholder_700x600.jpg'; ?>')">
							<div class="info">
								<p class="post-btn">View post</p>
								<p class="post-publish">
									<span class="publish-date"><?php echo get_the_date('D d, Y'); ?></span>
									<span class="separator">·</span>
									<span class="publish-time">1 Minute</span> 
								</p>
							</div>
						</figure>
						<footer class="post-footer">
							<?php $post_terms = get_the_terms(get_the_ID(), 'modelcategories'); ?>
							<?php
								$post_category = '';
								foreach($post_terms as $term):
									$post_category .= ', '.$term->name;
								endforeach;
								$post_category = trim($post_category, ',');
							?>
							<p class="post-category"><?php echo $post_category; ?></p>
							<h3 class="post-title"><?php the_title(); ?></h3>
						</footer>
					</a>
				</article>
			<?php endwhile; ?>
			</div>
		<?php else: ?>
			<h2 class="section-title lines">Nothing not found</h2>
		<?php endif; ?>
		</section>
<?php get_footer(); ?>