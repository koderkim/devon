<?php get_header(); ?>
		<!-- Hero -->
		<section class="hero over-header" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_1920x1200.jpg')">
			<div class="container">
				<div class="over-header-inner">
					<h1 class="title">Find Perfect Model</h1>
					<form class="searh-model" action="<?php echo get_permalink(25); ?>" method="POST">
						<span class="select-item">
							<select name="categories" id="categories" class="selectpicker">
								<option value="">Categories</option>
								<?php $terms = get_terms(array('taxonomy' => 'modelcategories', 'hide_empty' => false)); ?>
								<?php if($terms && ! is_wp_error($terms)): ?>
								<?php foreach($terms as $term): ?>
								<option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
								<?php endforeach; ?>
								<?php endif; ?>
							</select>
						</span>
						<span class="select-item">
							<select name="location" id="location" class="selectpicker">
								<option value="">Location</option>
								<?php $items = acf_get_field('model_location'); ?>
								<?php foreach($items['choices'] as $key => $val): ?>
								<option value="<?php echo $key; ?>"<?php echo $key == $model['model-location'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
								<?php endforeach; ?>
							</select>
						</span>
						<span class="select-item">
							<select name="background" id="background" class="selectpicker">
								<option value="">Background</option>
								<?php $items = acf_get_field('model_background'); ?>
								<?php foreach($items['choices'] as $key => $val): ?>
								<option value="<?php echo $key; ?>"<?php echo $key == $model['model-background'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
								<?php endforeach; ?>
							</select>
						</span>
						<span class="select-item">
							<select name="body" id="body" class="selectpicker">
								<option value="">Body</option>
								<?php $items = acf_get_field('model_body'); ?>
								<?php foreach($items['choices'] as $key => $val): ?>
								<option value="<?php echo $key; ?>"<?php echo $key == $model['model-body'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
								<?php endforeach; ?>
							</select>
						</span>
						<span class="select-item">
							<select name="donation" id="donation" class="selectpicker">
								<option value="">Donation</option>
								<?php $items = acf_get_field('model_donation'); ?>
								<?php foreach($items['choices'] as $key => $val): ?>
								<option value="<?php echo $key; ?>"<?php echo $key == $model['model-donation'] ? ' selected' : ''; ?>><?php echo $val; ?></option>
								<?php endforeach; ?>
							</select>
						</span>
						<input type="submit" value="Search" class="submit">
					</form>
				</div>
			</div>
		</section>
		<!-- End Hero -->
		<!-- Categories List -->
		<section class="container categories bigger">
			<?php $modelcatlist = get_terms(array('taxonomy' => array('modelcategories'), 'hide_empty' => false)); ?>
			<h2 class="section-title lines">Categories</h2>
			<div class="row categories-wr">
				<?php foreach($modelcatlist as $modelcat): ?>
				<?php $term = $modelcat->taxonomy.'_'.$modelcat->term_id; ?>
				<?php $bg = get_field('model_category_background', $term); ?>
				<?php
					$query = new WP_Query(array(
							'post_type' => 'model',
							'tax_query' => array(
								array(
									'taxonomy' => 'modelcategories',
									'field'    => 'term_id',
									'terms'    => $modelcat->term_id
								)
							),
							'order' => 'DESC',
							'orderby' => 'ID',
							'posts_per_page' => 1
					));
					$lastPostBg = null;
					while($query->have_posts()): $query->the_post();
						$gallery_list = explode(',', get_field('model_gallery'));
						if(count($gallery_list) > 0 && strlen($gallery_list[0])){
							$lastPostBg = wp_get_attachment_image_src($gallery_list[0], 'full');
						}
					endwhile;
					wp_reset_postdata();
					if($lastPostBg) $bg = $lastPostBg[0];
				?>
				<div class="col-sm-6 col-lg-4 categories-item-wr">
					<a href="<?php echo get_term_link($modelcat->term_id); ?>" class="category-models-item" style="background-image: url('<?php echo $bg ? $bg : get_bloginfo('template_url').'/assets/img/placeholder/placeholder_400x350.jpg';  ?>')">
						<footer class="categories-item-footer">
							<p class="category"><?php echo $modelcat->name; ?></p>
							<p class="models-count"><?php echo $modelcat->count; ?> models</p>
						</footer>
					</a>
				</div>
				<?php endforeach; ?>
			</div>
		</section>
		<!-- End Categories List -->
		<!-- Review -->
		<?php /* ?><div class="container-fluid full-screen-quote">
			<div class="row">
				<div class="col-md-6">
					<div class="full-screen-quote-text">
						<p class="quotes">“</p>
						<p class="description">Proin gravida nibh vel velit auctor aliquet. Aenean 
							sollicitudin, lorem quis bibendum auctor, nisi elit conse quat ipsum, nec sagittis sem nibh id elit duis sed.
						</p>
						<address class="author">Jena Stain</address>
					</div>
				</div>
				<div class="col-md-6 author-img-wr">
					<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_900x500.jpg" alt="" class="author-img">
				</div>
			</div>
		</div><?php */ ?>
		<!-- End Review -->
		<!-- Popular Models List -->
		<?php /* ?><section class="container popular-models model-row-wrap">
			<h2 class="section-title lines">Popular Models</h2>
			<article class="model-row">
				<div class="row">
					<div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 col-xl-2 offset-xl-0">
						<a href="05_Model_Profile.html" class="img-wr">
							<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_400x350.jpg" alt="">
						</a>
					</div>
					<div class="col-xl-6">
						<div class="row-model-info">
							<h3 class="title">
								<a href="05_Model_Profile.html">Alexis Rane</a>
							</h3>
							<div class="model-attr">
								<p class="attr-item">
									<span class="attr-name">Height:</span>
									<span class="attr-value">185</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Bust:</span>
									<span class="attr-value">85</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Waist:</span>
									<span class="attr-value">60</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Age:</span>
									<span class="attr-value">17</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Heir:</span>
									<span class="attr-value">Red</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Eyes:</span>
									<span class="attr-value">Blue</span>
								</p>
							</div>
							<p class="rating">
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
							</p>
						</div>
					</div>
					<div class="col-xl-2">
						<p class="soc-icons-wr">
							<a href="#" class="soc-icon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							<a href="#" class="soc-icon"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						</p>
					</div>
					<div class="col-xl-2">
						<p class="soc-icons-wr big">
							<a href="#" class="soc-icon"><i class="icon ion-android-favorite"></i></a>
							<a href="#" class="soc-icon"><i class="icon ion-ios-email-outline"></i></a>
						</p>
					</div>
				</div>
			</article>
			<article class="model-row">
				<div class="row">
					<div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 col-xl-2 offset-xl-0">
						<a href="05_Model_Profile.html" class="img-wr">
							<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_400x350.jpg" alt="">
						</a>
					</div>
					<div class="col-xl-6">
						<div class="row-model-info">
							<h3 class="title">
								<a href="05_Model_Profile.html">Alexis Rane</a>
							</h3>
							<div class="model-attr">
								<p class="attr-item">
									<span class="attr-name">Height:</span>
									<span class="attr-value">185</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Bust:</span>
									<span class="attr-value">85</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Waist:</span>
									<span class="attr-value">60</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Age:</span>
									<span class="attr-value">17</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Heir:</span>
									<span class="attr-value">Red</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Eyes:</span>
									<span class="attr-value">Blue</span>
								</p>
							</div>
							<p class="rating">
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
							</p>
						</div>
					</div>
					<div class="col-xl-2">
						<p class="soc-icons-wr">
							<a href="#" class="soc-icon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							<a href="#" class="soc-icon"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<a href="#" class="soc-icon"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</p>
					</div>
					<div class="col-xl-2">
						<p class="soc-icons-wr big">
							<a href="#" class="soc-icon"><i class="icon ion-android-favorite"></i></a>
							<a href="#" class="soc-icon"><i class="icon ion-ios-email-outline"></i></a>
						</p>
					</div>
				</div>
			</article>
			<article class="model-row">
				<div class="row">
					<div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 col-xl-2 offset-xl-0">
						<a href="05_Model_Profile.html" class="img-wr">
							<img src="<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_400x350.jpg" alt="">
						</a>
					</div>
					<div class="col-xl-6">
						<div class="row-model-info">
							<h3 class="title">
								<a href="05_Model_Profile.html">Alexis Rane</a>
							</h3>
							<div class="model-attr">
								<p class="attr-item">
									<span class="attr-name">Height:</span>
									<span class="attr-value">185</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Bust:</span>
									<span class="attr-value">85</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Waist:</span>
									<span class="attr-value">60</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Age:</span>
									<span class="attr-value">17</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Heir:</span>
									<span class="attr-value">Red</span>
								</p>
								<p class="attr-item">
									<span class="attr-name">Eyes:</span>
									<span class="attr-value">Blue</span>
								</p>
							</div>
							<p class="rating">
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
								<i class="fa fa-star active" aria-hidden="true"></i>
							</p>
						</div>
					</div>
					<div class="col-xl-2">
						<p class="soc-icons-wr">
							<a href="#" class="soc-icon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						</p>
					</div>
					<div class="col-xl-2">
						<p class="soc-icons-wr big">
							<a href="#" class="soc-icon"><i class="icon ion-android-favorite"></i></a>
							<a href="#" class="soc-icon"><i class="icon ion-ios-email-outline"></i></a>
						</p>
					</div>
				</div>
			</article>
		</section><?php */ ?>
		<!-- End Popular Models List -->
		<!-- Last Post List -->
		<?php /* ?><section class="container last-posts small">
			<h2 class="section-title lines">Journal</h2>
			<div class="row justify-content-center">
				<article class="col-md-6 col-lg-4 last-post-wr">
					<a href="13_Blog_Open_Widget.html" class="post-item no-decoration">
						<figure class="post-image" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_700x600.jpg')">
							<div class="info">
								<p class="post-btn">View post</p>
								<p class="post-publish">
									<span class="publish-date">Dec 25, 2017</span>
									<span class="separator">·</span>
									<span class="publish-time">1 Minute</span> 
								</p>
							</div>
						</figure>
						<footer class="post-footer">
							<p class="post-category">Agency</p>
							<h3 class="post-title">Perfect Fashion Show</h3>
						</footer>
					</a>
				</article>
				<article class="col-md-6 col-lg-4 last-post-wr">
					<a href="13_Blog_Open_Widget.html" class="post-item no-decoration">
						<figure class="post-image" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_700x600.jpg')">
							<div class="info">
								<p class="post-btn">View post</p>
								<p class="post-publish">
									<span class="publish-date">Dec 25, 2017</span>
									<span class="separator">·</span>
									<span class="publish-time">1 Minute</span>      
								</p>
							</div>
						</figure>
						<footer class="post-footer">
							<p class="post-category">Ashion</p>
							<h3 class="post-title">Changing Style WIth Makeup</h3>
						</footer>
					</a>
				</article>
				<article class="col-md-6 col-lg-4 last-post-wr">
					<a href="13_Blog_Open_Widget.html" class="post-item no-decoration">
						<figure class="post-image" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/img/placeholder/placeholder_700x600.jpg')">
							<div class="info">
								<p class="post-btn">View post</p>
								<p class="post-publish">
									<span class="publish-date">Dec 25, 2017</span>
									<span class="separator">·</span>
									<span class="publish-time">1 Minute</span>      
								</p>
							</div>
						</figure>
						<footer class="post-footer">
							<p class="post-category">Agency</p>
							<h3 class="post-title">Perfect Fashion Show</h3>
						</footer>
					</a>
				</article>
			</div>
		</section><?php */ ?>
		<!-- End Last Post List -->
<?php get_footer(); ?>