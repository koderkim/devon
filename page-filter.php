<?php
// Template name: page filter
?>

<?php get_header(); ?>
		<div class="container">
			<div class="row">
				<div class="col-12">
				<?php
					$meta_query = array(
						'relation' => 'AND'
					);
					if($_POST['location']){
						$meta_query[] = array(
							'key' => 'model_location',
							'value'	=> $_POST['location'],
							'compare' => '=',
						);
					}
					if($_POST['background']){
						$meta_query[] = array(
							'key' => 'model_background',
							'value'	=> $_POST['background'],
							'compare' => '=',
						);
					}
					if($_POST['body']){
						$meta_query[] = array(
							'key' => 'model_body',
							'value'	=> $_POST['body'],
							'compare' => '=',
						);
					}
					if($_POST['donation']){
						$meta_query[] = array(
							'key' => 'model_donation',
							'value'	=> $_POST['donation'],
							'compare' => '=',
						);
					}
					$args = array(
						'numberposts' => -1,
						'post_type'	=> 'model',
						'meta_query' => $meta_query
					);
					if($_POST['categories']){
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'modelcategories',
								'field'    => 'ID',
								'terms'    => $_POST['categories']
							)
						);
					}
					$query = new WP_Query($args);
					if($query->have_posts()):
						echo '<div class="row">';
						while($query->have_posts()): $query->the_post();
				?>
					<article class="col-md-6 col-lg-4 last-post-wr">
						<a href="<?php the_permalink(); ?>" class="post-item no-decoration">
							<?php $bg = get_the_post_thumbnail_url(); ?>
							<figure class="post-image" style="background-image: url('<?php echo $bg ? $bg : get_bloginfo('template_url').'/assets/img/placeholder/placeholder_700x600.jpg'; ?>')">
								<div class="info">
									<p class="post-btn">View post</p>
									<p class="post-publish">
										<span class="publish-date"><?php echo get_the_date('D d, Y'); ?></span>
										<span class="separator">·</span>
										<span class="publish-time">1 Minute</span> 
									</p>
								</div>
							</figure>
							<footer class="post-footer">
								<?php $post_terms = get_the_terms(get_the_ID(), 'modelcategories'); ?>
								<?php
									$post_category = '';
									foreach($post_terms as $term):
										$post_category .= ', '.$term->name;
									endforeach;
									$post_category = trim($post_category, ',');
								?>
								<p class="post-category"><?php echo $post_category; ?></p>
								<h3 class="post-title"><?php the_title(); ?></h3>
							</footer>
						</a>
					</article>
				<?php
						endwhile;
						echo '</div>';
					else:
						echo '<h2>Nothing not found</h2>';
					endif;
					wp_reset_postdata();
				?>
			</div>
		</div>
<?php get_footer(); ?>