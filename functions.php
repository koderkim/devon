<?php
if(function_exists('add_theme_support')){add_theme_support('post-thumbnails');}

remove_action('wp_head', 'wp_generator');

//require_once 'seo.php';

// function footer_enqueue_scripts(){
//    remove_action('wp_head','wp_print_scripts');
//     remove_action('wp_head','wp_print_head_scripts',9);
//     remove_action('wp_head','wp_enqueue_scripts',1);
//     add_action('wp_footer','wp_print_scripts',5);
//     add_action('wp_footer','wp_enqueue_scripts',5);
//     add_action('wp_footer','wp_print_head_scripts',5);
// }
// add_action('after_setup_theme','footer_enqueue_scripts');

function jquery_scripts_method(){
  // отменяем зарегистрированный jQuery
  // вместо "jquery-core" просто "jquery", чтобы отключить jquery-migrate
  wp_deregister_script('jquery-core');
  wp_register_script('jquery-core', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
  wp_enqueue_script('jquery');
}    
add_action('wp_enqueue_scripts', 'jquery_scripts_method');

// WP admin-ajax.php
/*function jsAjaxUrl(){
  $vars = array(
    'ajaxUrl' => admin_url('admin-ajax.php'),
    'is_mobile' => wp_is_mobile()
  );
  echo 
    '<script type="text/javascript">window.wpData = '.
    json_encode($vars).
    ';</script>';
}
add_action('wp_head', 'jsAjaxUrl');

function get_request_callback(){
  echo(json_encode( array('status'=>'ok','request_vars'=>$_REQUEST) ));
  wp_die();
}

add_action('wp_ajax_get_request', 'get_request_callback');
add_action('wp_ajax_nopriv_get_request', 'get_request_callback');*/
// End WP admin-ajax.php

register_nav_menus(array(
  'mainmenu' => __('Main menu','devon')
));


// if(function_exists('add_image_size')){
//   add_image_size('galerria', 50, 50, true);
// }
// add_filter('image_size_names_choose', 'my_custom_sizes');
// function my_custom_sizes($sizes){
//   return array_merge( $sizes, array(
//     'galerria' => 'Galleria',
//   ) );
// }

// if(function_exists('register_sidebar')){
//   register_sidebar(array(
//     'name' => 'Левый сайдбар',
//     'id' => 'sb-left',
//     'before_widget' => '<div id="%1$s" class="widget %2$s">',
//     'after_widget' => '</div>',
//     'before_title' => '<h2>',
//     'after_title' => '</h2>'
//   ))
// };


function kama_breadcrumbs( $sep='' ){

  global $post, $wp_query, $wp_post_types;
  // для локализации
  $l = array(
    'home' => 'Главная'
    ,'paged' => 'Страница %s'
    ,'404' => 'Ошибка 404'
    ,'search' => 'Результаты поиска по запросу - <b>%s</b>'
    ,'author' => 'Архив автора: <b>%s</b>'
    ,'year' => 'Архив за <b>%s</b> год'
    ,'month' => 'Архив за: <b>%s</b>'
    ,'day' => ''
    ,'attachment' => 'Медиа: %s'
    ,'tag' => 'Записи по метке: <b>%s</b>'
    ,'tax_tag' => '%s из "%s" по тегу: <b>%s</b>'
  );

  $w1 = '<div class="kama_breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">';
  $w2 = '</div>';
  $patt1 = '<span typeof="v:Breadcrumb"><a href="%s" rel="v:url" property="v:title">';
  $sep .= '</span>'; // закрываем span после разделителя!
  $patt = $patt1.'%s</a>';

  if( $paged = $wp_query->query_vars['paged'] ){
    $pg_patt = $patt1;
    $pg_end = '</a>'. $sep . sprintf($l['paged'], $paged);
  }

  $out = '';
  if( is_front_page() )
    return print $w1.($paged?sprintf($pg_patt, get_bloginfo('url')):'') . $l['home'] . $pg_end .$w2;

  elseif( is_404() )
    $out = $l['404']; 

  elseif( is_search() ){
    $out = sprintf( $l['search'], strip_tags($GLOBALS['s']) );
  }
  elseif( is_author() ){
    $q_obj = &$wp_query->queried_object;
    $out = ($paged?sprintf( $pg_patt, get_author_posts_url($q_obj->ID, $q_obj->user_nicename) ):'') . sprintf($l['author'], $q_obj->display_name) . $pg_end;
  }
  elseif( is_year() || is_month() || is_day() ){
    $y_url = get_year_link( $year=get_the_time('Y') );
    $m_url = get_month_link( $year, get_the_time('m') );
    $y_link = sprintf($patt, $y_url, $year);
    $m_link = sprintf($patt, $m_url, get_the_time('F'));
    if( is_year() )
      $out = ($paged?sprintf($pg_patt, $y_url):'') . sprintf($l['year'], $year) . $pg_end;
    elseif( is_month() )
      $out = $y_link . $sep . ($paged?sprintf($pg_patt, $m_url):'') . sprintf($l['month'], get_the_time('F')) . $pg_end;
    elseif( is_day() )
      $out = $y_link . $sep . $m_link . $sep . get_the_time('l');
  }

  // Страницы и древовидные типы записей
  elseif( $wp_post_types[$post->post_type]->hierarchical ){
    $parent = $post->post_parent;
    $crumbs=array();
    while($parent){
      $page = &get_post($parent);
      $crumbs[] = sprintf($patt, get_permalink($page->ID), $page->post_title);
      $parent = $page->post_parent;
    }
    $crumbs = array_reverse($crumbs);
    foreach ($crumbs as $crumb)
      $out .= $crumb.$sep;
    $out = $out . $post->post_title;
  }
  else // Таксономии, вложения и не древовидные типы записей
  {
    // Определяем термины
    if( is_singular() ){
      if( ! $taxonomies ){
        $taxonomies = get_taxonomies( array('hierarchical' => true, 'public' => true) );
        if( count( $taxonomies ) == 1 ) $taxonomies = 'category';
      }
      if( $term = get_the_terms( $post->post_parent ? $post->post_parent : $post->ID, $taxonomies ) ){
        $term = array_shift( $term );
      }
    }
    else
      $term = &$wp_query->get_queried_object();

    if( ! $term && ! is_attachment() )
      return print "Error: Taxonomy is not defined!"; 

    $pg_term_start = ($paged && $term->term_id) ? sprintf( $pg_patt, get_term_link( (int)$term->term_id, $term->taxonomy ) ) : '';

    if( is_attachment() ){
      if(!$post->post_parent)
        $out = sprintf($l['attachment'], $post->post_title);
      else
        $out = crumbs_tax($term->term_id, $term->taxonomy, $sep, $patt) . sprintf($patt, get_permalink($post->post_parent), get_the_title($post->post_parent) ).$sep.$post->post_title;
    }
    elseif( is_single() )
      $out = crumbs_tax($term->parent, $term->taxonomy, $sep, $patt) . sprintf($patt, get_term_link( (int)$term->term_id, $term->taxonomy ), $term->name). $sep.$post->post_title;
    // Метки, архивная страница типа записи, произвольные одноуровневые таксономии
    elseif( ! is_taxonomy_hierarchical( $term->taxonomy ) ){
      // метка
      if( is_tag() )
        $out = $pg_term_start . sprintf($l['tag'], $term->name) . $pg_end;
      // архивная страница произвольного типа записи
      elseif( !$term->term_id ) 
        $home_after = sprintf($patt, '/'. $term->name, $term->label). $pg_end;
      // таксономия
      else {
        $post_label = $wp_post_types[$post->post_type]->labels->name;
        $tax_label = $GLOBALS['wp_taxonomies'][$term->taxonomy]->labels->name;
        $out = $pg_term_start . sprintf($l['tax_tag'], $post_label, $tax_label, $term->name) .  $pg_end;
      }
    }
    // Рубрики и таксономии
    else
      $out = crumbs_tax($term->parent, $term->taxonomy, $sep, $patt) . $pg_term_start . $term->name . $pg_end;
  }

  // замена ссылки на архивную страницу для типа записи 
  if( $post->post_type == 'book' )
    $home_after = sprintf($patt, '/about_book', 'Книжки'). $sep;

  // ссылка на архивную страницу произвольно типа поста
  if( ! $home_after && ! empty($post->post_type) && $post->post_type != 'post' && !is_page() && !is_attachment() )
    $home_after = sprintf($patt, '/'. $post->post_type, $wp_post_types[$post->post_type]->labels->name ). $sep;

  $home = sprintf($patt, get_bloginfo('url'), $l['home'] ). $sep . $home_after;

  return print $w1. $home . $out .$w2;
}
function crumbs_tax($term_id, $tax, $sep, $patt){
  $termlink = array();
  while( (int)$term_id ){
    $term2 = &get_term( $term_id, $tax );
    $termlink[] = sprintf($patt, get_term_link( (int)$term2->term_id, $term2->taxonomy ), $term2->name). $sep;
    $term_id = (int)$term2->parent;
  }
  $termlinks = array_reverse($termlink);
  return implode('', $termlinks);
}

function kama_pagenavi($before='', $after='', $echo=true) {  
  /* ================ Настройки ================ */  
  $text_num_page = ''; // Текст для количества страниц. {current} заменится текущей, а {last} последней. Пример: 'Страница {current} из {last}' = Страница 4 из 60  
  $num_pages = 5; // сколько ссылок показывать  
  $stepLink = ''; // после навигации ссылки с определенным шагом (значение = число (какой шаг) или '', если не нужно показывать). Пример: 1,2,3...10,20,30  
  $dotright_text = '…'; // промежуточный текст "до".  
  $dotright_text2 = '…'; // промежуточный текст "после".  
  $backtext = '<'; // текст "перейти на предыдущую страницу". Ставим '', если эта ссылка не нужна.  
  $nexttext = '>'; // текст "перейти на следующую страницу". Ставим '', если эта ссылка не нужна.  
  $first_page_text = ''; // текст "к первой странице" или ставим '', если вместо текста нужно показать номер страницы.  
  $last_page_text = ''; // текст "к последней странице" или пишем '', если вместо текста нужно показать номер страницы.  
  /* ================ Конец Настроек ================ */   

  global $wp_query;  
  $posts_per_page = (int) $wp_query->query_vars[posts_per_page];  
  $paged = (int) $wp_query->query_vars[paged];  
  $max_page = $wp_query->max_num_pages;  

  if($max_page <= 1 ) return false; //проверка на надобность в навигации  

  if(empty($paged) || $paged == 0) $paged = 1;  

  $pages_to_show = intval($num_pages);  
  $pages_to_show_minus_1 = $pages_to_show-1;  

  $half_page_start = floor($pages_to_show_minus_1/2); //сколько ссылок до текущей страницы  
  $half_page_end = ceil($pages_to_show_minus_1/2); //сколько ссылок после текущей страницы  

  $start_page = $paged - $half_page_start; //первая страница  
  $end_page = $paged + $half_page_end; //последняя страница (условно)  

  if($start_page <= 0) $start_page = 1;  
  if(($end_page - $start_page) != $pages_to_show_minus_1) $end_page = $start_page + $pages_to_show_minus_1;  
  if($end_page > $max_page) {  
      $start_page = $max_page - $pages_to_show_minus_1;  
      $end_page = (int) $max_page;  
  }  

  if($start_page <= 0) $start_page = 1;  

  $out='';
      $out.= $before."<div id='pagination' class='clearfix'>\n";  
      if ($text_num_page){  
          $text_num_page = preg_replace ('!{current}|{last}!','%s',$text_num_page);  
          $out.= sprintf ("<span class='pages'>$text_num_page</span>",$paged,$max_page);  
      }  

      if ($backtext && $paged!=1) $out.= '<a href="'.get_pagenum_link(($paged-1)).'" class="prev"><span>'.$backtext.'</span></a>';  

      if ($start_page >= 2 && $pages_to_show < $max_page) {  
          $out.= '<a href="'.get_pagenum_link().'"><span>'. ($first_page_text?$first_page_text:1) .'</span></a>';  
          if($dotright_text && $start_page!=2) $out.= '<span>'.$dotright_text.'</span>';  
      }  

      for($i = $start_page; $i <= $end_page; $i++) {  
          if($i == $paged) {  
              $out.= '<span class="current">'.$i.'</span>';  
          } else {  

              $out.= '<a href="'.get_pagenum_link($i).'"><span>'.$i.'</span></a>';  
          }  
      }  

      //ссылки с шагом  
      if ($stepLink && $end_page < $max_page){  
          for($i=$end_page+1; $i<=$max_page; $i++) {  
              if($i % $stepLink == 0 && $i!==$num_pages) {  
                  if (++$dd == 1) $out.= '<span>'.$dotright_text2.'</span>';  
                  $out.= '<a href="'.get_pagenum_link($i).'"><span>'.$i.'</span></a>';  
              }  
          }  
      }  

      if ($end_page < $max_page) {  
          if($dotright_text && $end_page!=($max_page-1)) $out.= '<span>'.$dotright_text2.'</span>';  
          $out.= '<a href="'.get_pagenum_link($max_page).'"><span>'. ($last_page_text?$last_page_text:$max_page) .'</span></a>';  
      }  

      if ($nexttext && $paged!=$end_page) $out.= '<a href="'.get_pagenum_link(($paged+1)).'" class="next"><span>'.$nexttext.'</span></a>';  

      $out.= "</div>".$after."\n";  
  if ($echo) echo $out;  
  else return $out;  
}
// gzip
// function enable_zlib(){
//     ini_set('zlib.output_compression', 'On');
//     ini_set('zlib.output_compression_level', '1');
// }
// add_action('init', 'enable_zlib');

// function shortcode_func($atts, $content=""){
//    return 'shortcode';
// }
// add_shortcode('attension', 'shortcode_func');


// Колонка ID
if (is_admin()) {
  // колонка "ID" для таксономий (рубрик, меток и т.д.) в админке
  foreach (get_taxonomies() as $taxonomy){
    add_action("manage_edit-${taxonomy}_columns", 'tax_add_col');
    add_filter("manage_edit-${taxonomy}_sortable_columns", 'tax_add_col');
    add_filter("manage_${taxonomy}_custom_column", 'tax_show_id', 10, 3);
  }
  add_action('admin_print_styles-edit-tags.php', 'tax_id_style');
  function tax_add_col($columns) {return $columns + array ('tax_id' => 'ID');}
  function tax_show_id($v, $name, $id) {return 'tax_id' === $name ? $id : $v;}
  function tax_id_style() {print '<style>#tax_id{width:4em}</style>';}

  // колонка "ID" для постов и страниц в админке
  add_filter('manage_posts_columns', 'posts_add_col', 5);
  add_action('manage_posts_custom_column', 'posts_show_id', 5, 2);
  add_filter('manage_pages_columns', 'posts_add_col', 5);
  add_action('manage_pages_custom_column', 'posts_show_id', 5, 2);
  add_action('admin_print_styles-edit.php', 'posts_id_style');
  function posts_add_col($defaults) {$defaults['wps_post_id'] = __('ID'); return $defaults;}
  function posts_show_id($column_name, $id) {if ($column_name === 'wps_post_id') echo $id;}
  function posts_id_style() {print '<style>#wps_post_id{width:4em}</style>';}
}

add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar(){
  if(!current_user_can('administrator') && !is_admin()){
    show_admin_bar(false);
  }
}

// Comment reply
// function enqueue_comment_reply(){
//   if(is_singular() && comments_open() && (get_option('thread_comments') == 1)) 
//     wp_enqueue_script('comment-reply');
// }
// add_action('wp_enqueue_scripts', 'enqueue_comment_reply');

// add http if not (ftp://, ftps://, http://, https://)
function addhttp($url){
  if(!preg_match("~^(?:f|ht)tps?://~i", $url)){
    $url = "http://".$url;
  }
  return $url;
}

if('disable_gutenberg'){
  add_filter('use_block_editor_for_post_type', '__return_false', 100);
  add_action('admin_init', function(){
    remove_action('admin_notices', ['WP_Privacy_Policy_Content', 'notice']);
    add_action('edit_form_after_title', ['WP_Privacy_Policy_Content', 'notice']);
  });
}

function devon_customizer_init($wp_customize){
  $wp_customize->add_section(
    'devon_options',
    array(
      'title'     => 'Contacts',
      'priority'  => 200,
      'description' => 'Site contacts'
    )
  );
  // Facebook
  $wp_customize->add_setting(
    'devon_social_fb',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'devon_social_fb',
    array(
      'section'  => 'devon_options',
      'label'    => 'Facobook',
      'description' => '(url)',
      'type'     => 'text'
    )
  );
  // Instagram
  $wp_customize->add_setting(
    'devon_social_instagram',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'devon_social_instagram',
    array(
      'section'  => 'devon_options',
      'label'    => 'Instagram',
      'description' => '(url)',
      'type'     => 'text'
    )
  );
  // Tweeter
  $wp_customize->add_setting(
    'devon_social_tw',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'devon_social_tw',
    array(
      'section'  => 'devon_options',
      'label'    => 'Tweeter',
      'description' => '(url)',
      'type'     => 'text'
    )
  );
  // Pinterest
  $wp_customize->add_setting(
    'devon_social_pinterest',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'devon_social_pinterest',
    array(
      'section'  => 'devon_options',
      'label'    => 'Pinterest',
      'description' => '(url)',
      'type'     => 'text'
    )
  );
}
add_action('customize_register', 'devon_customizer_init');

// Type Models
add_action('init', 'create_devon_model_taxonomies', 0);
function create_devon_model_taxonomies(){
  $labels = array(
    'name' => 'Categories',
    'singular_name' => 'Categories',
    'search_items' =>  'Search categories',
    'popular_items' => 'Popular categories',
    'all_items' => 'All categories',
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => 'Edit category',
    'update_item' => 'Update category',
    'add_new_item' => 'Add new category',
    'new_item_name' => 'New category name',
    'separate_items_with_commas' => 'Separate writers with commas',
    'add_or_remove_items' => 'Add or remove category',
    'choose_from_most_used' => 'Choose from the most used writers',
    'menu_name' => 'Сategories of Models',
  );
 
  register_taxonomy('modelcategories', 'models', array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'models'),
  ));
}

add_action('init', 'create_devon_models_tags', 0);
function create_devon_models_tags(){
  $labels = array(
    'name' => 'Tags',
    'singular_name' => 'Tags',
    'search_items' =>  'Search tags',
    'popular_items' => 'Popular tags',
    'all_items' => 'All tags',
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => 'Edit tag',
    'update_item' => 'Update tag',
    'add_new_item' => 'Add new tag',
    'new_item_name' => 'New tag name',
    'separate_items_with_commas' => 'Separate writers with commas',
    'add_or_remove_items' => 'Add or remove tags',
    'choose_from_most_used' => 'Choose from the most used writers',
    'menu_name' => 'Tags of models',
  );
 
  register_taxonomy('modeltags', 'models', array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'modeltags'),
  ));
}

add_action('init', 'devon_model_init');
function devon_model_init()
{
  $labels = array(
    'name' => 'Models',
    'singular_name' => 'Model',
    'add_new' => 'Add Model',
    'add_new_item' => 'Add New Model',
    'edit_item' => 'Edit Model',
    'new_item' => 'New Model',
    'view_item' => 'View Model',
    'search_items' => 'Search Model',
    'not_found' =>  'Models not found',
    'not_found_in_trash' => 'In cart Models not found',
    'parent_item_colon' => '',
    'menu_name' => 'Models'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'taxonomies' => array('modelcategories', 'modeltags'),
    'menu_icon' => 'dashicons-universal-access',
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','author','thumbnail','excerpt','comments')
  );
  register_post_type('model',$args);
}

add_filter('post_updated_messages', 'devon_models_updated_messages');
function devon_models_updated_messages($messages){
  global $post, $post_ID;

  $messages['models'] = array(
    0 => '',
    1 => sprintf('Models updated. <a href="%s">Vew model</a>', esc_url(get_permalink($post_ID))),
    2 => 'Custom field updated.',
    3 => 'Custom field removed.',
    4 => 'Models updated.',
    5 => isset($_GET['revision']) ? sprintf('Model restored from revision %s', wp_post_revision_title((int) $_GET['revision'], false)) : false,
    6 => sprintf('Model posted. <a href="%s">Go to model</a>', esc_url(get_permalink($post_ID))),
    7 => 'Model saved.',
    8 => sprintf('Model saved. <a target="_blank" href="%s">Preview Model</a>', esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
    9 => sprintf('Model is scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Model</a>',
      date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
    10 => sprintf('Model draft updated. <a target="_blank" href="%s">Preview Model</a>', esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
  );

  return $messages;
}

function devon_add_rewrite_rules($rules){
  $new = array();
  $new['model/(.+)/?$'] = 'index.php?model=$matches[1]';
  $new['models/(.+)/?$'] = 'index.php?modelcategories=$matches[1]';

  return array_merge($new, $rules);
}
add_filter('rewrite_rules_array', 'devon_add_rewrite_rules');

// $role = get_role('editor');
// $role->remove_cap('edit_posts');
// $role->remove_cap('edit_published_posts');
// $role->remove_cap('publish_posts');
// $role->remove_cap('delete_posts');
// $role->remove_cap('delete_published_posts');

function get_meta_values($key = '', $type = 'post', $status = 'publish'){
    global $wpdb;
    
    if(empty($key)) return;
    
    $r = $wpdb->get_col($wpdb->prepare("
        SELECT DISTINCT(pm.meta_value) FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s' 
        AND p.post_type = '%s'
        AND p.post_status = '%s' 
        ORDER BY pm.meta_value
      ", $key, $type, $status));
    
    return $r;
}

?>